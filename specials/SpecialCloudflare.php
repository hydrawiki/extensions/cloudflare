<?php
/**
 * Curse Inc.
 * Cloudflare
 * Purges cloudflare cache when varnish/squid purge requests are sent.
 *
 * @author		Cameron Chunn
 * @copyright	(c) 2016 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		Cloudflare
 * @link		https://gitlab.com/hydrawiki
 *
**/

class SpecialCloudflare extends SpecialPage {
	/**
	 * Main Constructor
	 *
	 * @access	public
	 * @return	void
	 */
	public function __construct() {
		parent::__construct(
			'Cloudflare', // name
			'cloudflare', // required user right
			true // display on Special:Specialpages
		);
	}

	/**
	 * Main Executor
	 *
	 * @access	public
	 * @param	string	Sub page passed in the URL.
	 * @return	void	[Outputs to screen]
	 */
	public function execute( $path ) {
		global $wgCloudflareEmail, $wgCloudflareApiKey;
		$this->setHeaders();
		$this->checkPermissions();
		$this->outputHeader();

		if (!$wgCloudflareEmail || !$wgCloudflareApiKey) {
			$this->getOutput()->showErrorPage("an_error","config_error");

		}

		$cf = new Cloudflare($wgCloudflareEmail, $wgCloudflareApiKey);

		try {
			$test = $cf->get('/user');
		} catch (Exception $e) {
			$this->getOutput()->showErrorPage("an_error","generic_error_output",["<h3>Connection Error</h3><pre>".$e->getMessage()."</pre>"]);
			return;
		}


		if (count($test['errors'])) {
			$this->getOutput()->showErrorPage("an_error","generic_error_output",["<h3>Connection Error</h3><pre>".print_r($test['errors'], 1)."</pre>"]);
		} else {
			$this->getOutput()->addHTML("<p>Your configuration settings appear to be correct, and you are connected to the Cloudflare API.</p>
									<h3>Connected User Info</h3><hr />
										<table>
											<tbody>
											<tr>
												<th>Email</th>
												<td>".$test['result']['email']."</td>
											</tr>");
			if(count($test['result']['organizations'])) {
				foreach ($test['result']['organizations'] as $org) {
					if ($org['name'] !== "SELF") {
						$this->getOutput()->addHTML("<tr>
							<th>Organization</th>
							<td>".$org['name']."
						");
						if (count($org['roles'])) {
							$this->getOutput()->addHTML("<ul>");
							foreach ($org['roles'] as $r) {
								$this->getOutput()->addHTML("<li>".$r."</li>");
							}
							$this->getOutput()->addHTML("</ul>");
						}
						$this->getOutput()->addHTML("</td></tr>");
					}
				}
			}
			$this->getOutput()->addHTML("</tbody></table>");
		}



	}

	/**
	 * Return the group name for this special page.
	 *
	 * @access protected
	 * @return string
	 */
	protected function getGroupName() {
		return 'other'; //Change to display in a different category on Special:SpecialPages.
	}
}
