<?php
/**
 * Curse Inc.
 * Cloudflare
 * Purges cloudflare cache when varnish/squid purge requests are sent.
 *
 * @author		Cameron Chunn
 * @copyright	(c) 2016 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		Cloudflare
 * @link		https://gitlab.com/hydrawiki
 *
**/

class Cloudflare {

    public $email;
    public $key;

    /**
     * Setup variables.
     * @param string $email account email
     * @param string $key   api key
     */
    public function __construct($email, $key) {
        $this->email = $email;
        $this->key = $key;
    }

	/**
	 * Call into self to purge URLs in an array
	 * @param  array $urls
	 * @return void
	 */
	public function purgeUrls($urls) {
		$failedZones = [];
		$zoneIds = [];
		$runURLs = [];

		foreach ($urls as $url) {
			$host = parse_url($url, PHP_URL_HOST);
			$host = str_replace("www.", "", $host); // Strip www. if its in the name

			// If we dont have the zone id, lets find it.
			if (!isset($zoneIds[$host]) && !in_array($host, $failedZones)) {
				$findZone = $this->get('/zones', ['name' => $host]); // search for ZoneID
				if ( !isset( $findZone['result'][0]['id'] ) ) {
					$failedZones[] = $host; // add to fail list. Dont keep recalling this api endpoint.
					continue; // skip this record, we cant find its zone. Bad Domain? Not on Cloudflare?
				}
				// Add zoneid for host.
				$zoneIds[$host] = $findZone['result'][0]['id'];
			}

			// Initialized empty url list holder, if not set.
			if (!isset($runURLs[$host])) {
				$runURLs[$host] = [];
			}

			$runURLs[$host][] = $url;
		}

		// loop through URLs, and chunk.
		foreach ($runURLs as $host => $urlList) {
			$zoneId = isset($zoneIds[$host])?$zoneIds[$host]:false;
			if (!$zoneId) {
				continue;
			}

			// Cloudflare API limits purge_cache requests to 30 files at a time.
			$chunks = array_chunk($urlList, 30);
			foreach ($chunks as $urlChunk) {
				$this->delete('/zones/'.$zoneId.'/purge_cache', [
					"files" => $urlChunk
				]);
			}
		}
	}

    /**
     * Overloading function to dynamically handle function calls to the class.
     * @param  string $method   The Method Called (allowed: delete, get, patch)
     * @param  array $args      The Arguments Passed (to be sent to the API as data)
     * @return array         data returned by API call
     */
    public function __call($method, $args) {

        // Determin call method
        $callMethod = strtoupper($method);
        if (!in_array($callMethod, ['DELETE', 'GET', 'PATCH'])) {
            throw new Exception("The method (".$method.") is not valid.", 1);
        }

        // Verify first argument is an endpoint.
        if (!isset($args[0]) || !is_string($args[0])) {
            throw new Exception("This call requires an endpoint to be set. Example: \$clouldflare->".$method."('/user')", 1);
        }

        $endpoint = $args[0];

        // append leading / if its missing from the call
        if (substr($endpoint, 0, 1) !== "/") {
            $endpoint = "/".$endpoint;
        }

        $postData = isset($args[1])?$args[1]:[];

        if (!is_array($postData)) {
            throw new Exception("Second parameter expected to be array.", 1);
        }

        $basePath = "https://api.cloudflare.com/client/v4";

        $callUrl = $basePath.$endpoint;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $callMethod);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            "X-Auth-Email: ".$this->email,
            "X-Auth-Key: ".$this->key,
            "Content-Type: application/json"
        ]);
        if ($postData) {
            if ($callMethod == "GET") {
                $queryStr = http_build_query($postData);
                $callUrl = $callUrl."?".$queryStr;
            } else {
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postData));
            }
        }

        curl_setopt($ch, CURLOPT_URL, $callUrl);
        $output = curl_exec($ch);
        curl_close($ch);

        $jsonOutput = json_decode($output, 1);
        if (!is_array($jsonOutput)) {
            throw new Exception("Invalid JSON returned by call -- Received Message: ".$output, 1);
        }

        return $jsonOutput;
    }



}
