<?php
/**
 * Curse Inc.
 * Cloudflare
 * Purges cloudflare cache when varnish/squid purge requests are sent.
 *
 * @author		Cameron Chunn
 * @copyright	(c) 2016 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		Cloudflare
 * @link		https://gitlab.com/hydrawiki
 *
**/

class CloudflareHooks {

	/**
	 * onPageContentSaveComplete, get SquidURL's and send them to cloudflare for purging.
	 * @param  [type]  $article   [description]
	 * @param  [type]  $user      [description]
	 * @param  [type]  $content   [description]
	 * @param  [type]  $summary   [description]
	 * @param  boolean $isMinor   [description]
	 * @param  boolean $isWatch   [description]
	 * @param  [type]  $section   [description]
	 * @param  [type]  $flags     [description]
	 * @param  [type]  $revision  [description]
	 * @param  [type]  $status    [description]
	 * @param  [type]  $baseRevId [description]
	 * @return [type]             [description]
	 */
	public static function onPageContentSaveComplete( $article, $user, $content, $summary, $isMinor, $isWatch, $section, $flags, $revision, $status, $baseRevId ) {
		self::doArticlePurge($article);
	}

	/**
	 * onArticlePurge, get SquidURL's and send them to cloudflare for purging.
	 * @param  WikiPage $article
	 * @return void
	 */
	public static function onArticlePurge( &$article ) {
		self::doArticlePurge($article);
	}

	/**
	 * Purge an article's URLs
	 * @param  WikiPage $article
	 * @return void
	 */
	public static function doArticlePurge( $article ) {
		global $wgCloudflareEmail, $wgCloudflareApiKey;
		if (empty($wgCloudflareEmail) || empty($wgCloudflareApiKey)) {
			return; // can't proceed.
		}

		$cf = new Cloudflare($wgCloudflareEmail, $wgCloudflareApiKey);
		$urls = $article->getTitle()->getSquidURLs();

		try {
			$cf->purgeUrls($urls);
		} catch (Exception $e) {
			// dont hold up the end user. It doesn't actually matter to them. Just log the error.
			wfWarn($e->getMessage());
		}
	}
}
